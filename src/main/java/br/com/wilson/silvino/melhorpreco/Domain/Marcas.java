/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.wilson.silvino.melhorpreco.Domain;

import java.io.Serializable;
import javax.persistence.Column;

/**
 *
 * @author sala308b
 */
public class Marcas extends GenericDomain implements Serializable{
    @Column(nullable = false, length = 50)
    private String nome;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    
    
    
}
