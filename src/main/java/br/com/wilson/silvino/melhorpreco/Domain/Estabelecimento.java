/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.wilson.silvino.melhorpreco.Domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Diamond
 */
@Entity
public class Estabelecimento extends GenericDomain implements Serializable{
    
      @Column(nullable = false, length = 50)
    private String nome;
      
      @Column(nullable = false)
 @Temporal(TemporalType.DATE)
    private Date horario_inicio;
    
  @Column(nullable = true)
   @Temporal(TemporalType.DATE)
    private Date horario_fim;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Date getHorario_inicio() {
        return horario_inicio;
    }

    public void setHorario_inicio(Date horario_inicio) {
        this.horario_inicio = horario_inicio;
    }

    public Date getHorario_fim() {
        return horario_fim;
    }

    public void setHorario_fim(Date horario_fim) {
        this.horario_fim = horario_fim;
    }
  
  
    
}
