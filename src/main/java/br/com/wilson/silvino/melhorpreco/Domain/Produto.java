/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.wilson.silvino.melhorpreco.Domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Diamond
 */

@Entity
public class Produto extends GenericDomain implements Serializable{
     @Column(nullable = false, length = 50)
    private String codigoProduto ; 
    
    @Column(nullable = false, length = 50)
    private String marca ;
    
     @Column(nullable = false, length = 50)
    private String descricao ;
     
     @Column(nullable = false)
 @Temporal(TemporalType.DATE)
    private Date validade;

    public String getCodigoProduto() {
        return codigoProduto;
    }

    public void setCodigoProduto(String codigoProduto) {
        this.codigoProduto = codigoProduto;
    }

    
     

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Date getValidade() {
        return validade;
    }

    public void setValidade(Date validade) {
        this.validade = validade;
    }
     
     
}
