<%-- 
    Document   : newjsp
    Created on : Feb 6, 2018, 1:58:13 AM
    Author     : wilso
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Marcas</title>
        <meta charset="utf-8">
        <link href="../resources/css/simpleGridTemplate.css" rel="stylesheet" type="text/css"/>
        <link href="../resources/css/style.css" rel="stylesheet" type="text/css"/>







    </head>



    <body>

        <p><dd><li><a href="../home.xhtml">HOME</a></li></dd></p>


<div class="gallery">
    <div class="thumbnail"><a href="#"><img src="../resources/images/bra.jpg" alt="" width="10" style="width:200px; class="cards"/></a>
        <h4>Brahama 

            é uma marca de cerveja brasileira criada em 1888, no Rio de Janeiro, pela Manufactura de Cerveja Brahma Villiger & Companhia, que depois mudaria de nome para Companhia Cervejaria Brahma, e depois seria sucedida pela AmBev. A cerveja Brahma é a segunda marca de cerveja mais consumida no Brasil, e a nona cerveja mais consumida em todo o mundo. No Brasil, é considerada a 3ª marca mais valiosa do país, avaliada em 4,3 bilhões de dólares.</h4>

        <a href="https://www.brahma.com.br/" target="_blank">Saiba Mais...</a>
    </div>

    <div class="thumbnail"><a href="#"><img src="../resources/images/Bud.jpg" alt="" width="10" style="width:200px; class="cards"/></a>
        <h4>Budweiser, também conhecida popularmente como Bud, é uma cerveja do tipo Lager americana, fabricada pela empresa Anheuser-Busch, fundada em 1876 por imigrantes alemães. Tem sua versão light, a Bud light.


            A Budweiser é a patrocinadora oficial do lutador de MMA brasileiro Anderson Silva. Internacionalmente é patrocinadora de importantes ligas profissionais, como futebol americano, baseball e NBA, além de ter forte presença na NASCAR.</h4>

        <a href="https://www.budweiser.com.br/" target="_blank">Saiba Mais...</a>
    </div>


    <div class="thumbnail"><a href="#"><img src="../resources/images/Stella.jpg" alt="" width="10" style="width:200px; class="cards"/></a>
        <h4>A Stella Artois é uma cerveja pilsner lager (de baixa fermentação) premium, que tem suas origens em uma tradicional cervejaria chamada Den Hoorn (O Chifre), que data em 1366, uma das mais antigas do mundo[carece de fontes].

            Fabricada desde 1926 na cidade de Leuven, no interior da Bélgica, berço de algumas dos melhores mestres cervejeiros do mundo, a Stella Artois é uma cerveja super premium, de sabor balanceado e marcante.</h4>

        <a href="https://www.stellaartois.com.br/age-gate" target="_blank">Saiba Mais...</a>
    </div>


    <div class="thumbnail"><a href="#"><img src="../resources/images/Estrella.jpg" alt="" width="10" style="width:200px; class="cards"/></a>
        <h4>Estrella Galicia Nossa história começa em 1906, das mãos de um arrojado empreendedor, José Mª Rivera Corral, que após retornar da América funda a fábrica “Estrella de Galicia”, dedicada a elaboração de gelo e por um então exótico negócio, a cerveja.</h4>

        <a href="https://estrellagalicia.com.br/companhia/" target="_blank">Saiba Mais...</a>
    </div>

    <div class="gallery">
        <div class="thumbnail"><a href="#"><img src="../resources/images/corona.jpg" alt="" width="10" style="width:200px; class="cards"/></a>
            <h4>Corona Extra é a marca de cerveja mais popular do México, fundada em 1925 e foi a segunda cerveja a ser produzida pelo Grupo Modelo. Em 2012 a multinacional Belga-brasileira Anheuser-Busch InBev comprou o Grupo Modelo, dona da Cerveja Corona Extra.</h4>

            <a href="https://www.coronaextrabrasil.com.br/" target="_blank">Saiba Mais...</a>
        </div>

        <div class="thumbnail"><a href="#"><img src="../resources/images/heineken.jpg" alt="" width="10" style="width:200px; class="cards"/></a>
            <h4>Heineken International é uma cervejaria holandesa, fundada em 1864 por Wandscheer Heineken na cidade de Amsterdã. Heineken possui cerca de 140 cervejarias em mais de 70 países, empregando aproximadamente 85.000 pessoas.</h4>

            <a href="https://www.heineken.com/br/agegateway?returnurl=%2fbr" target="_blank">Saiba Mais...</a>
        </div>


        <div class="thumbnail"><a href="#"><img src="../resources/images/Bohemia.jpg" alt="" width="10" style="width:200px; class="cards"/></a>
            <h4>A Cervejaria Bohemia é uma empresa brasileira fabricante de cerveja e a primeira cervejaria do país. Foi fundada em 1853 na cidade de Petrópolis, estado do Rio de Janeiro, pelo colono alemão Henrique Kremer, na época artista com o nome de cervejaria Bohemia. Quando ele faleceu em 1865, a empresa ficou com os seus herdeiros, que a rebatizaram de Augusto Kremer & Cia. </h4>

            <a href="https://www.bohemia.com.br/" target="_blank">Saiba Mais...</a>
        </div>


        <div class="thumbnail"><a href="#"><img src="../resources/images/Skol.jpg" alt="" width="10" style="width:200px; class="cards"/></a>
            <h4>Skol é uma marca de cerveja de propriedade de Hyvan Leak do Coroado, da empresa dinamarquesa Carlsberg, com licença para ser fabricada no Brasil. Seu nome vem da palavra escandinava skål, que significa "à vossa saúde/à nossa saúde"; expressão comum que antecede brindes. </h4>

            <a href="https://www.skol.com.br/" target="_blank">Saiba Mais...</a>
        </div>

    </div>

</div>




</body>
</html>
